@extends('dashboard.index')
@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Kandidat Pelamar</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="col">

            </div>
        </div>
    </div>
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong></strong> {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong></strong> {{ session('failed') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row d-flex justify-content-between mb-3">


    </div>
    <div class="row">
        @foreach ($kandidat as $kandidat)

            <div class="col-11 col-md-8 lowongan-item mb-3">
                <div class="d-flex justify-content-between">
                    <h5 class="judul-lowongan">{{ $kandidat->lowongan->judul }}</h5>
                    <a href="/storage/{{ $kandidat->cv }}" class="">
                        <h6>Lihat CV</h6>
                    </a>
                </div>
                <p>Tanggal lamaran {{ date('d M Y', strtotime($kandidat->created_at)) }}</p>
                <div class="d-flex justify-content-between">
                    <div>

                        <h6>Nama : {{ $kandidat->nama }} </h6>
                        <h6>Email: {{ $kandidat->email }}</h6>
                        <h6>No Hp: {{ $kandidat->no_hp }}</h6>
                    </div>
                    <div class="align-self-end">
                        <a href="/dashboard/kandidat/{{ $kandidat->id }}/accept">Terima</a> | <a
                            href="/dashboard/kandidat/{{ $kandidat->id }}/reject">Tolak</a>
                    </div>
                </div>

            </div>
        @endforeach
    </div>

@endsection
