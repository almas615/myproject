<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/dashboard">
                    <span data-feather="home"></span>
                    Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/dashboard/kandidat">
                    <span data-feather="users"></span>
                    Kandidat
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/dashboard/lowongan">
                    <span data-feather="layers"></span>
                    Lowongan
                </a>
            </li>
        </ul>


    </div>
</nav>
