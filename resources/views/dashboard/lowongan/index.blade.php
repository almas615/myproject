@extends('dashboard.index')
@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Lowongan Kerja</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="col">
                <a href="/dashboard/lowongan/create" class="btn btn-primary btn-sm float-right mr-auto ml-auto">Tambah
                    Lowongan</a>
            </div>
        </div>
    </div>
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Yeay!!</strong> {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Oh no!!</strong> {{ session('failed') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row d-flex justify-content-between mb-3">


    </div>
    <div class="row">
        @foreach ($lowongan as $lowongan)

            <div class="col-11 col-md-8 lowongan-item mb-3">
                <div class="d-flex justify-content-between">
                    <h5 class="judul-lowongan">{{ $lowongan->judul }}</h5>
                    <a href="" class="">
                        <h6>OPEN</h6>
                    </a>
                </div>
                <p>{{ date('d M Y', strtotime($lowongan->tanggal_mulai)) }} -
                    {{ date('d M Y', strtotime($lowongan->tanggal_selesai)) }}</p>
                <div class="d-flex justify-content-between">

                    <p>{{ count($lowongan->kandidat) }} Kandidat</p>
                    <div>
                        <a href="/dashboard/lowongan/{{ $lowongan->id }}/edit">Edit</a> | <a
                            href="/dashboard/lowongan/{{ $lowongan->id }}/delete">Hapus</a>
                    </div>
                </div>

            </div>
        @endforeach
    </div>

@endsection
