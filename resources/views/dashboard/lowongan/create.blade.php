@extends('dashboard.index')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Yeay!!</strong> {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session()->has('failed'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Oh no!!</strong> {{ session('failed') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Tambah Lowongan</h1>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>


    <div class="row mt-5">
        <form action="/dashboard/lowongan" method="post" style="width: 100%">
            @csrf
            <div class="mb-3 col-12 col-md-6 d-inline-block">
                <label for="judul" class="form-label">Judul</label>
                <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" id="judul"
                    placeholder="" value="{{ old('judul') }}">
                @error('judul')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-4 d-inline-block">
                <label for="status" class="form-label">Status</label>
                <select class="form-select @error('status') is-invalid @enderror" id="status" name="status"
                    aria-label="Default select example">
                    <option value=""> -- pilih --</option>
                    <option value="draft" @if (old('status') == 'draft') selected @endif>Draft</option>
                    <option value="publish" @if (old('status') == 'publish') selected @endif>Publish</option>
                    <option value="close" @if (old('status') == 'close') selected @endif>Close</option>
                </select>
                @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-3 col-12 col-md-6 d-inline-block">
                <label for="tipe_pekerjaan" class="form-label">Tipe Pekerjaan</label>
                <select class="form-select @error('tipe_pekerjaan') is-invalid @enderror " id="tipe_pekerjaan"
                    name="tipe_pekerjaan" aria-label="Default select example">
                    <option value=""> -- pilih --</option>
                    <option value="Fulltime-Onsite" @if (old('tipe_pekerjaan') == 'Fulltime-Onsite')
                        selected
                        @endif>Fulltime-Onsite</option>
                    <option value="Fulltime-Remote" @if (old('tipe_pekerjaan') == 'Fulltime-Remote')
                        selected
                        @endif>Fulltime-Remote</option>
                    <option value="Fulltime-Onsite/Remote" @if (old('tipe_pekerjaan') == 'Fulltime-Onsite/Remote')
                        selected @endif>Fulltime-Onsite/Remote</option>
                    <option value="Part-time" @if (old('tipe_pekerjaan') == 'Part-time') selected @endif>Part-time</option>
                    <option value="Project-based" @if (old('tipe_pekerjaan') == 'Project-based') selected @endif>
                        Project-based</option>
                </select>
                @error('tipe_pekerjaan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-3 col-12 col-md-2 d-inline-block">
                <label for="tgl_mulai" class="form-label">Tanggal Mulai Pendaftaran</label>
                <input type="date" class="form-control @error('tanggal_mulai') is-invalid @enderror" name="tanggal_mulai"
                    id="tgl_mulai" placeholder="" value="{{ old('tanggal_mulai') }}">
                @error('tanggal_mulai')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-2 d-inline-block">
                <label for="tgl_selesai" class="form-label">Tanggal Akhir Pendaftaran</label>
                <input type="date" class="form-control @error('tanggal_selesai') is-invalid @enderror"
                    name="tanggal_selesai" id="tgl_selesai" placeholder="" value="{{ old('tanggal_selesai') }}">
                @error('tanggal_selesai')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-4 d-inline-block">
                <label for="gaji" class="form-label">Gaji</label>
                <input type="text" class="form-control @error('gaji') is-invalid @enderror" name="gaji" id="gaji"
                    placeholder="" value="{{ old('gaji') }}">
                @error('gaji')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-2 d-inline-block">
                <label for="per" class="form-label">per</label>
                <input type="text" class="form-control" id="per" placeholder="bulan" readonly>
            </div>
            <div class="mb-3 col-12 col-md-4 d-inline-block">
                <label for="skill" class="form-label">Skill yang harus dimiliki</label>
                <input type="text" class="form-control @error('skill') is-invalid @enderror" name="skill" id="skill"
                    placeholder="" value="{{ old('skill') }}">
                @error('skill')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-10">
                <label for="deskripsi" class="form-label">Deskripsi</label>
                <textarea class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" name="deskripsi"
                    rows="5">{{ old('deskripsi') }}</textarea>
                @error('deskripsi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>

                @enderror
            </div>
            <div class="mb-3 col-12 col-md-10 d-flex justify-content-end">
                <button type="submit" class="btn btn-dark" style="margin-left: auto">Simpan</button>
            </div>
        </form>
    </div>

@endsection
