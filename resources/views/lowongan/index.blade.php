@extends('index')
@section('content')
    <div class="row d-flex justify-content-between mb-3">
        <h3 class="col-md-8">
            List Lowongan Kerja
        </h3>
    </div>
    <div class="row">
        @foreach ($lowongan as $lowongan)

            <div class="col-11 col-md-8 lowongan-item mb-3">
                <div class="d-flex justify-content-between">
                    <h5 class="judul-lowongan">{{ $lowongan->judul }}</h5>
                    <a href="" class="">
                        <h6 class="text-success me-4">OPEN</h6>
                    </a>
                </div>
                <p>{{ date('d M Y', strtotime($lowongan->tanggal_mulai)) }} -
                    {{ date('d M Y', strtotime($lowongan->tanggal_selesai)) }}</p>
                <div class="d-flex justify-content-end">
                    <div class="me-4 mb-3">
                        <a href="/lowongan/{{ $lowongan->id }}" class="btn btn-dark" style="width: 100px">Apply</a>
                    </div>
                </div>

            </div>
        @endforeach
    </div>

@endsection
