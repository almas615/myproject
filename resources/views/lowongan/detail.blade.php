@extends('index')
@section('content')
    <div class="row mb-3 d-flex justify-content-center">
        <div class="col-12 col-md-9">
            @if (session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Yeay!!</strong> {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (session()->has('failed'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Oh no!!</strong> {{ session('failed') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="judul d-flex justify-content-between">
                <div class="col-auto col-md-6">
                    <h2>
                        {{ $lowongan->judul }}
                    </h2>
                    <p class="">{{ $lowongan->tipe_pekerjaan }}</p>
                </div>
                <div class="col-auto col-md-2">
                    <a href="/kandidat/create?lowongan={{ $lowongan->id }}" class="btn btn-dark"
                        style="width: 150px">Apply</a>
                </div>
            </div>
            <div class="gaji">

                <h5>Gaji</h5>
                <p>Rp. {{ number_format($lowongan->gaji) }} / bulan</p>
            </div>
            <div class="skill">

                <h5>Skill yang harus dimililki</h5>
                <p>{{ $lowongan->skill }}</p>
            </div>
            <div class="deskripsi col-md-8">

                <h5>Deskripsi</h5>
                <p>{{ $lowongan->deskripsi }}</p>
            </div>
        </div>
    </div>

@endsection
