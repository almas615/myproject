@extends('index')
@section('content')
    <div class="row mb-3 d-flex justify-content-center">
        <div class="col-12 col-md-9">
            <div class="judul d-flex flex-column justify-content-between">
                <div class="col-auto col-md-12">
                    <h2>
                        Registrasi Pelamar
                    </h2>

                </div>
                <div class="col-auto col-md-12 mt-3">
                    <form action="/kandidat?lowongan={{ request('lowongan') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Nama </label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                                    name="nama" placeholder="Nama Lengkap" value="{{ auth()->user()->name }}">
                                @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="email" class="form-label">Email address</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                    placeholder="Email" id="email" name="email" value="{{ auth()->user()->email }}">
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <label for="no_hp" class="form-label">No. Telp/Watsapp</label>
                                <input type="text" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp"
                                    name="no_hp" placeholder="nohp ">
                                @error('no_hp')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="pendidikan" class="form-label">Pendidikan Terakhir</label>
                                <select class="form-select @error('pendidikan') is-invalid @enderror"
                                    aria-label="Default select example" id="pendidikan" name="pendidikan">
                                    <option value="">-- Pilih --</option>
                                    <option value="SMP/MTS">SMP/MTS</option>
                                    <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                </select>
                                @error('pendidikan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <label for="bidang_studi" class="form-label">Bidang Studi</label>
                                <input type="text" class="form-control @error('bidang_studi') is-invalid @enderror"
                                    id="bidang_studi" name="bidang_studi" placeholder="bidang studi">
                                @error('bidang_studi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="daerah_asal" class="form-label">Daerah Asal</label>
                                <input type="text" class="form-control @error('daerah_asal') is-invalid @enderror"
                                    id="daerah_asal" name="daerah_asal" placeholder="daerah_asal">
                                @error('daerah_asal')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <label for="social_instagram" class="form-label">Social Media - Instagram</label>
                                <input type="text" class="form-control @error('social_instagram') is-invalid @enderror"
                                    id="social_instagram" name="social_instagram" placeholder="Instagram URL">
                                @error('social_instagram')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="social_facebook" class="form-label">Social Media - Facebook</label>
                                <input type="text" class="form-control @error('social_facebook') is-invalid @enderror"
                                    id="social_facebook" name="social_facebook" placeholder="Facebook URL">
                                @error('social_facebook')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <label for="social_git" class="form-label">Akun Gitlab/Github, dsb</label>
                                <input type="text" class="form-control @error('social_git') is-invalid @enderror"
                                    id="social_git" name="social_git" placeholder="GIT URL">
                                @error('social_git')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="cv" class="form-label">Upload CV(File PDF)</label>
                                <input class="form-control @error('cv') is-invalid @enderror" type="file" id="formFile"
                                    name="cv">
                                @error('cv')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                        </div>
                        <div class="row mb-2">

                            <button type="submit" class="btn btn-dark position-relative"
                                style="width: 100px;left: 55vw;">Apply</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
