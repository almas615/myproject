<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLowongansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lowongan', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->enum('tipe_pekerjaan', ['Fulltime-Onsite', 'Fulltime-Remote', 'Fulltime-Onsite/Remote', 'Project-based', 'Part-time']);
            $table->enum('status', ['draft', 'publish', 'close']);
            $table->integer('gaji');
            $table->text('deskripsi');
            $table->string('skill');
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan');
    }
}
