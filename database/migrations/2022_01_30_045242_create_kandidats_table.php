<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKandidatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kandidat', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_lowongan');
            $table->foreign('id_lowongan')->references('id')->on('lowongan');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('nama');
            $table->string('email');
            $table->string('no_hp');
            $table->enum('pendidikan', ['SMP/MTS', 'SMA/SMK/MA', 'D3', 'S1']);
            $table->string('bidang_studi');
            $table->string('daerah_asal');
            $table->string('social_instagram');
            $table->string('social_facebook');
            $table->string('social_git');
            $table->string('cv');
            $table->enum('status', ['ditolak', 'diterima', 'diproses'])->default('diproses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kandidat');
    }
}
