<?php

namespace Database\Seeders;
// import kandidat model
use App\Models\Kandidat;
use Illuminate\Database\Seeder;

class KandidatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create kandidat
        Kandidat::create([
            'id_lowongan' => 1,
            'nama' => 'Almas Adlilwafi',
            'email' => 'almasadlilwafi615@gmail.com',
            'no_hp' => '081212121212',
            'pendidikan' => 'S1',
            'bidang_studi' => 'Teknik Informatika',
            'daerah_asal' => 'Jakarta',
            'social_instagram' => 'almasadlilwafi',
            'social_facebook' => 'almasadlilwafi',
            'social_git' => 'almasadlilwafi',
            'cv' => 'almasadlilwafi',
            'status' => 'diproses',
        ]);
        Kandidat::create([
            'id_lowongan' => 1,
            'nama' => 'wafi',
            'email' => 'wafi615@gmail.com',
            'no_hp' => '081212121212',
            'pendidikan' => 'S1',
            'bidang_studi' => 'Teknik Informatika',
            'daerah_asal' => 'Jakarta',
            'social_instagram' => 'almasadlilwafi',
            'social_facebook' => 'almasadlilwafi',
            'social_git' => 'almasadlilwafi',
            'cv' => 'almasadlilwafi',
            'status' => 'diproses',
        ]);
        Kandidat::create([
            'id_lowongan' => 2,
            'nama' => 'adlil',
            'email' => 'adlil615@gmail.com',
            'no_hp' => '081212121212',
            'pendidikan' => 'S1',
            'bidang_studi' => 'Teknik Informatika',
            'daerah_asal' => 'Jakarta',
            'social_instagram' => 'almasadlilwafi',
            'social_facebook' => 'almasadlilwafi',
            'social_git' => 'almasadlilwafi',
            'cv' => 'almasadlilwafi',
            'status' => 'diproses',
        ]);
    }
}
