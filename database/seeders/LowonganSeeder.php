<?php

namespace Database\Seeders;
// lowongan model import
use App\Models\Lowongan;


use Illuminate\Database\Seeder;

class LowonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //lowongan create
        Lowongan::create([
            'judul' => 'Web Developer',
            'tipe_pekerjaan' => 'Fulltime-Onsite',
            'status' => 'publish',
            'gaji' => '5000000',
            'deskripsi' => 'Sed tempor aliquet letius conubia et congue facilisis lectus senectus enim sem aptent porta eleifend nullam vestibulum faucibus eget maximus dui nec etiam morbi torquent cursus efficitur habitasse turpis sit ridiculus donec metus taciti finibus nibh ultricies ad id magnis vitae posuere ligula montes lobortis suscipit nisl elementum odio phasellus consectetur dictum egestas diam lacus blandit imperdiet mauris pharetra fusce',
            'skill' => 'php, laravel, javascript, html, css',
            'tanggal_mulai' => '2020-01-01',
            'tanggal_selesai' => '2020-01-04',
        ]);
        Lowongan::create([
            'judul' => 'Sisitem Analis',
            'tipe_pekerjaan' => 'Fulltime-Onsite',
            'status' => 'publish',
            'gaji' => '5000000',
            'deskripsi' => 'Sed tempor aliquet letius conubia et congue facilisis lectus senectus enim sem aptent porta eleifend nullam vestibulum faucibus eget maximus dui nec etiam morbi torquent cursus efficitur habitasse turpis sit ridiculus donec metus taciti finibus nibh ultricies ad id magnis vitae posuere ligula montes lobortis suscipit nisl elementum odio phasellus consectetur dictum egestas diam lacus blandit imperdiet mauris pharetra fusce',
            'skill' => 'php, laravel, javascript, html, css',
            'tanggal_mulai' => '2020-01-01',
            'tanggal_selesai' => '2020-01-04',
        ]);
    }
}
