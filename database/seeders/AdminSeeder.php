<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// import Admin model
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create admin
        Admin::create([
            'name' => 'Admin',
            'email' => 'almasadlilwafi689@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
