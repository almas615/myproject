<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use Laravel\Socialite\Facades\Socialite;
use App\Models\Kandidat;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    //return view login
    public function login()
    {
        return view('auth.login');
    }
    //redirect to provider
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }
    //make function callback
    public function callback(Request $request)
    {
        $user_google = Socialite::driver('google')->user();
        $user = User::where('email', $user_google->email)->first();
        if ($user) {
            Auth::login($user);
            $request->session()->regenerate();
            return redirect()->intended('/');
        } else {
            $user = new User;
            $user->name = $user_google->name;
            $user->email = $user_google->email;
            $user->password = bcrypt('123456');
            $user->save();
            Auth::login($user);
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
    }
    //logout
    public function logout()
    {
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/');
    }
}
