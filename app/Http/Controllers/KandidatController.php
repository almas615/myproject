<?php

namespace App\Http\Controllers;

use App\Mail\MyEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\Kandidat;
use Illuminate\Http\Request;

class KandidatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view with all kandidat and his lowongan
        $kandidat = Kandidat::with('lowongan')->where('status', "=", 'diproses')->get();
        return view('dashboard.kandidat.index', [
            'kandidat' => $kandidat
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //cek if user exist in kandidat
        $kandidat = Kandidat::where('id_user', auth()->user()->id)->where('id_lowongan', request('lowongan'))->first();
        //if kandidat true redirect to view with error
        if ($kandidat) {
            return redirect('/lowongan/' . $kandidat->id_lowongan)->with('failed', 'Anda sudah melamar di lowongan ini');
        }
        //return view
        return view('lowongan.registrasi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data request
        $validatedData = $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'pendidikan' => 'required',
            'bidang_studi' => 'required',
            'daerah_asal' => 'required',
            'social_instagram' => 'required',
            'social_facebook' => 'required',
            'social_git' => 'required',
            'cv' => 'required|file|mimes:pdf|max:2048',
        ]);

        //if request file cv is not empty
        if ($request->file('cv')) {
            $validatedData['cv'] = $request->file('cv')->store('cv');
        }
        $validatedData['status'] = 'diproses';
        $validatedData['id_lowongan'] = $request->lowongan;
        $validatedData['id_user'] = auth()->user()->id;
        //try to create kandidat
        try {
            Kandidat::create($validatedData);
            return redirect('/lowongan/' . $request->lowongan)->with('success', 'Registrasi berhasil');
        } catch (\Exception $e) {
            return redirect('/lowongan/' . $request->lowongan)->with('failed', 'Registrasi gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function show(Kandidat $kandidat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function edit(Kandidat $kandidat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kandidat $kandidat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kandidat $kandidat)
    {
        //
    }

    //function accept
    public function accept(Kandidat $kandidat)
    {
        //try to update status kandidat
        try {
            $kandidat->update([
                'status' => 'diterima'
            ]);
            return redirect('/dashboard/kandidat/' . $kandidat->id . '/sendEmail')->with('success', 'Kandidat diterima');
        } catch (\Exception $e) {
            return redirect('/dashboard/kandidat')->with('failed', 'Kandidat gagal diterima');
        }
    }
    //function reject
    public function reject(Kandidat $kandidat)
    {
        //try to update status kandidat
        try {
            $kandidat->update([
                'status' => 'ditolak'
            ]);
            return redirect('/dashboard/kandidat/' . $kandidat->id . '/sendEmail')->with('success', 'Kandidat ditolak');
        } catch (\Exception $e) {
            return redirect('/dashboard/kandidat/sendEmail')->with('failed', 'Kandidat gagal ditolak');
        }
    }
    //send email to kandidat
    public function sendEmail(Kandidat $kandidat)
    {
        //try to send email
        try {
            if ($kandidat->status == 'diterima') {
                $data = 'Selamat, anda telah diterima di lowongan ' . $kandidat->lowongan->judul;
            } else {
                $data = 'Maaf, anda tidak diterima di lowongan ' . $kandidat->lowongan->judul;
            }
            Mail::to($kandidat->email)->send(new MyEmail($data));
            return redirect('/dashboard/kandidat')->with('success', 'Email berhasil dikirim');
        } catch (\Exception $e) {
            return redirect('/dashboard/kandidat')->with('failed', 'Email gagal dikirim');
        }
    }
}
