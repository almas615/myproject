<?php

namespace App\Http\Controllers;

use App\Models\Lowongan;
use Illuminate\Http\Request;

class LowonganController extends Controller
{

    public function index()
    {
        //get all lowongan wiht his prosessing kandidat
        $lowongan = Lowongan::with(['kandidat' => function ($query) {
            $query->where('status', 'diproses');
        }])->get();
        return view('dashboard.lowongan.index', [
            'lowongan' => $lowongan
        ]);
    }

    public function indexMain()
    {
        //get all lowongan wiht his kandidat
        $lowongan = Lowongan::all()->where('status', '=', 'publish');
        return view('lowongan.index', [
            'lowongan' => $lowongan
        ]);
    }

    public function create()
    {
        //return view
        return view('dashboard.lowongan.create');
    }

    public function store(Request $request)
    {
        //validate
        $request->validate([
            'judul' => 'required',
            'tipe_pekerjaan' => 'required',
            'status' => 'required',
            'gaji' => 'required',
            'deskripsi' => 'required',
            'skill' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
        ]);
        //create all request data
        $data = $request->all();
        //create lowongan
        Lowongan::create($data);
        //redirect
        return redirect("/dashboard/lowongan")->with('success', 'Lowongan berhasil ditambahkan');
    }


    public function show(Lowongan $lowongan)
    {
        //
    }
    // show for main page
    public function showMain(Lowongan $lowongan)
    {
        //return view
        return view('lowongan.detail', [
            'lowongan' => $lowongan
        ]);
    }


    public function edit(Lowongan $lowongan)
    {

        //return view
        return view('dashboard.lowongan.edit', [
            'lowongan' => $lowongan,
        ]);
    }

    public function update(Request $request, Lowongan $lowongan)
    {
        //validate
        $request->validate([
            'judul' => 'required',
            'tipe_pekerjaan' => 'required',
            'status' => 'required',
            'gaji' => 'required',
            'deskripsi' => 'required',
            'skill' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
        ]);
        //update all request data
        $data = $request->all();
        //update table lowongan
        $lowongan->update($data);
        //redirect
        return redirect("/dashboard/lowongan")->with('success', 'Lowongan berhasil diubah');
    }


    public function destroy(Lowongan $lowongan)
    {
        //try catch
        try {
            $lowongan->delete();
            return redirect("/dashboard/lowongan")->with('success', 'Lowongan berhasil dihapus');
        } catch (\Exception $e) {
            return redirect("/dashboard/lowongan")->with('failed', 'Lowongan gagal dihapus');
        }
    }
}
