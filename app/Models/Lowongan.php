<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    use HasFactory;
    protected $table = 'lowongan';

    protected $fillable = [
        'judul',
        'tipe_pekerjaan',
        'status',
        'gaji',
        'deskripsi',
        'skill',
        'tanggal_mulai',
        'tanggal_selesai',
    ];

    // associate with kandidat
    public function kandidat()
    {
        return $this->hasMany(Kandidat::class, 'id_lowongan');
    }
}
