<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kandidat extends Model
{
    use HasFactory;
    protected $table = 'kandidat';

    protected $fillable = [
        'id_lowongan',
        'id_user',
        'nama',
        'email',
        'no_hp',
        'pendidikan',
        'bidang_studi',
        'daerah_asal',
        'social_instagram',
        'social_facebook',
        'social_git',
        'cv',
        'status',
    ];

    // associate with lowongan
    public function lowongan()
    {
        return $this->belongsTo(Lowongan::class, 'id_lowongan');
    }
    //associate with user
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
