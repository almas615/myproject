<?php

use Illuminate\Support\Facades\Route;
//import controllers
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KandidatController;
use App\Http\Controllers\LowonganController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
//Route lowongan
Route::get('/lowongan', [LowonganController::class, 'indexMain']);
Route::get('/lowongan/{lowongan}', [LowonganController::class, 'showMain']);
//route kandidat
Route::get('/kandidat/create', [KandidatController::class, 'create'])->middleware('auth');
Route::post('/kandidat', [KandidatController::class, 'store'])->middleware('auth');


//prefix auth
Route::prefix('auth')->group(function () {
    Route::get('/redirect', [AuthController::class, 'redirect'])->middleware('guest');
    Route::get('/callback', [AuthController::class, 'callback']);
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/login', function () {
        return view('auth.login');
    })->middleware('guest')->name('login');
});



//prefix dashboard
Route::prefix('dashboard')->group(function () {
    Route::get('/', function () {
        return view('dashboard.home.index');
    });
    //lowongan Route
    Route::get('/lowongan', [LowonganController::class, 'index']);
    Route::post('/lowongan', [LowonganController::class, 'store']);
    Route::get('/lowongan/create', [LowonganController::class, 'create']);
    Route::get('/lowongan/{lowongan}/delete', [LowonganController::class, 'destroy']);
    Route::get('/lowongan/{lowongan}/edit', [LowonganController::class, 'edit'])->name('lowongan.edit');
    Route::post('/lowongan/{lowongan}/update', [LowonganController::class, 'update'])->name('lowongan.update');

    //kandidat Route
    Route::get('/kandidat', [KandidatController::class, 'index']);
    Route::get('/kandidat/{kandidat}/accept', [KandidatController::class, 'accept']);
    Route::get('/kandidat/{kandidat}/reject', [KandidatController::class, 'reject']);
    Route::get('/kandidat/{kandidat}/sendEmail', [KandidatController::class, 'sendEmail']);
});
